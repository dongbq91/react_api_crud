import axios from "axios";
const student_url = "http://localhost:8080/api/v1/student";
class studentService{

    getStudents(){
        return axios.get(student_url);
    }
    createStudent(student){
        return axios.post(student_url,student);
    }
    getStudentById(id){
        return axios.get(student_url+"/"+ id);
    }
    updateStudent(id){
        return axios.put(student_url+"/"+id);
    }
    deleteStudent(id){
        return axios.delete(student_url+"/"+id);
    }
}
export  default new studentService()