import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import ListStudentComponent from "./components/ListStudentComponent";
import CreateStudentComponent from "./components/createStudentComponent";


function App() {
  return (
      <div>
          <div className="container">
              <ListStudentComponent />
          </div>

          <div>
              <CreateStudentComponent/>
          </div>
      </div>

  );
}

export default App;
