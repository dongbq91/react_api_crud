import React, {Component} from 'react';
import studentService from "../services/studentService";

class UpdateStudentComponent extends Component {
    constructor(props) {
        super(props)
        this.state={
            name: ''
        }
        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.savestudent = this.saveStudent.bind(this);
    }
    changeNameHandler = (event) =>{
        this.state({name:event.target.value})
    }
    saveStudent =(e) => {
        e.preventDefault();
        let student = {name: this.state.name};
        console.log('student=>'+JSON.stringify(student));
        studentService.updateStudent(student).then(res =>{
            this.props.history.push('/student');
        });
    }
    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">

                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h1> Create Student</h1>
                            <div className="card-body">
                                <form>
                                    <div className= "form-group">
                                        <label>Student Name: </label>
                                        <input placeholder="Name" name="Name" className="form-control" value={this.state.name} onChange={this.changeNameHandler} />
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveStudent}>Save</button>
                                    <button className="btn btn-danger " onClick={this.cancel.bind(this)} style={{marginLeft: "10px "}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateStudentComponent;