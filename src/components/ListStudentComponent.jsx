import React, {Component} from 'react';
import studentService from "../services/studentService";

class ListStudentComponent extends Component {
    constructor(props) {
        super(props);
        this.state={
            student: []
        }
        this.addStudent = this.addStudent.bind(this);
    }
    componentDidMount() {
        studentService.getStudents().then((res)=>{
            this.setState({student: res.data});
        });
    }
    addStudent(){
        this.props.history.push('/add-student');
    }
    render() {
        return (
            <div>
                <h2 className="text-center"> List Student</h2>
                <div className= "row ">
                    <div>
                        <button className="btn btn-primary" onClick={this.addStudent}>Create</button>
                    </div>
                    <table className="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th> Student ID </th>
                            <th> Student name </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.student.map(
                                studentEntity =>
                                    <tr key={studentEntity.id}>
                                        <td>{studentEntity.id}</td>
                                        <td>{studentEntity.name}</td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListStudentComponent;